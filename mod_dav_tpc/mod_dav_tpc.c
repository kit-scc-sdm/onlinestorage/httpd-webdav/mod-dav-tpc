#define APR_WANT_STRFUNC
#include "apr_want.h"
#include "apr_errno.h"
#include "apr_lib.h"
#include "apr_uri.h"
#include "apr_optional.h"
#include "apr_strings.h"
#include "apr_tables.h"
#include "apr_time.h"
#include "apr_pools.h"
#include "ap_config.h"
#include "httpd.h"
#include "http_config.h"
#include "http_log.h"
#include "http_protocol.h"
#include "mod_dav.h"
#include "mod_log_config.h"
#include <gfal_api.h>

// FUTR: Consider using apr_cstr_casecmp added in apr_cstr.h added in APR 1.6.0 which only
//       interprets ASCII.

#define DAV_TPC_VERSION "0.1.0"

/**
 * Value of r->handler if the request is to be handled by mod_dav.
 * Value taken from HTTPD's modules/dav/main/mod_dav.c.
 */
#define DAV_HANDLER_NAME "dav-handler"

// Declare module, data is filled in at the end of the file.
module AP_MODULE_DECLARE_DATA dav_tpc_module;

/**
 * Size of the buffer allocated for getting the error string for a status via
 * strerror, apr_strerror or dtpc_strerror.
 */
#define ERROR_STRING_BUF_SIZE 256

/**
 * Custom error space built on top of APR's error codes in the range [453000, 453100).
 *
 * The range was a randomly-chosen 100-interval within APR's USERERR space.
 */
#define DTPC_START_ERROR (APR_OS_START_USERERR + 453000)
#define DTPC_ERROR_SPACE_SIZE 100

// Custom error/status values:

/** Scheme part of (or associated with) an URI is unknown or invalid. */
#define DTPC_UNKNOWN_SCHEME   (DTPC_START_ERROR + 1)
/** Port is not given explicitly and cannot be determined otherwise (e.g. from scheme). */
#define DTPC_UNDETERMINABLE_PORT   (DTPC_START_ERROR + 2)
/** URI is not valid for this kind of processing. */
#define DTPC_INVALID_URI   (DTPC_START_ERROR + 3)
/** Request is malformed according to to HTTP (and related) specifications. */
#define DTPC_MALFORMED_HTTP_REQUEST   (DTPC_START_ERROR + 4)
/** A syntax error was found during parsing of a "params" string in the configuration. */
#define DTPC_PARAMS_PARSING_ERROR   (DTPC_START_ERROR + 5)

static char *dtpc_status_to_string(apr_status_t status) {
    if (status < DTPC_START_ERROR || status >= DTPC_START_ERROR + DTPC_ERROR_SPACE_SIZE)
        return "Status not in the range of mod_dav_tpc, no error string known";

    switch (status) {
    case DTPC_UNKNOWN_SCHEME:
        return "URI scheme is unknown or invalid";
    case DTPC_UNDETERMINABLE_PORT:
        return "Port is not given explicitly and cannot be determined otherwise";
    case DTPC_INVALID_URI:
        return "URI is not valid for this kind of processing";
    case DTPC_MALFORMED_HTTP_REQUEST:
        return "Request is malformed according to to HTTP specifications";
    case DTPC_PARAMS_PARSING_ERROR:
        return "Error during parsing of a \"params\" string (syntax error)";
    default:
        return "Error string not specified yet";
    }
}

static char *dtpc_strerror(apr_status_t status, char *buf, apr_size_t bufsize) {
    if (status < DTPC_START_ERROR || status >= DTPC_START_ERROR + DTPC_ERROR_SPACE_SIZE) {
        return apr_strerror(status, buf, bufsize);
    } else {
        apr_cpystrn(buf, dtpc_status_to_string(status), bufsize);
        return buf;
    }
}

static const char *dtpc_strerror_pool(apr_pool_t *pool, apr_status_t status) {
    char *buf = (char *) apr_palloc(pool, ERROR_STRING_BUF_SIZE);
    ap_assert(buf != NULL);
    dtpc_strerror(status, buf, ERROR_STRING_BUF_SIZE);
    return buf;
}

/**
 * Status value enriching apr_status_t with an optional message.
 *
 * Message should extend on status or give context, not just describe status.
 */
typedef struct {
    /** Status code. */
    apr_status_t status;
    /** Optional human-readable error message (may be NULL). */
    const char *message;
} enr_status_t;

/**
 * Success value of enr_status_t, return this directly from functions.
 */
static const enr_status_t enr_status_success = {
    .status = APR_SUCCESS,
    .message = NULL,
};

typedef enum {
    DTPC_ACTION_UNSET = 0,    /**< unset, no meaning, zero value */
    DTPC_ACTION_PULL  = 1<<0, /**< pull, GET from remote endpoint */
    DTPC_ACTION_PUSH  = 1<<1, /**< push, PUT to remote endpoint */
} action_e;

const char *action_e_to_name(action_e value) {
    switch (value) {
    case DTPC_ACTION_UNSET: return "unset";
    case DTPC_ACTION_PULL: return "pull";
    case DTPC_ACTION_PUSH: return "push";
    }
    return "[invalid value]";
}

typedef struct {
    /** The name of the scheme */
    const char *name;
    /** The default port for the scheme */
    apr_port_t default_port;
} scheme_t;

// Port 1 is not used by any common service, so it's relatively safe.
// The service registered with IANA for port 1 has been deprecated by RFC 7805.
#define UNSET_PORT 1

/**
 * List of all schemes which are supported by the default-shipped gfal2 plugins.
 *
 * The list is based on a manual review of the source code at v2.21.1.
 *
 * Missing: "file", "mock".
 *
 * Idea and implementation based on apr_uri_port_of_scheme().
 */
static const scheme_t schemes[] = {
    {"dcap",    UNSET_PORT},
    {"gsidcap", UNSET_PORT},
    {"gsiftp",  UNSET_PORT},
    {"ftp",     UNSET_PORT},
    {"http",    80},
    {"https",   443},
    {"dav",     80},
    {"davs",    443},
    {"s3",      UNSET_PORT},
    {"s3s",     UNSET_PORT},
    {"gcloud",  UNSET_PORT},
    {"gclouds", UNSET_PORT},
    {"swift",   UNSET_PORT},
    {"swifts",  UNSET_PORT},
    {"cs3",     UNSET_PORT},
    {"cs3s",    UNSET_PORT},
    {"lfn",     UNSET_PORT},
    {"lfc",     UNSET_PORT},
    {"rfio",    UNSET_PORT},
    {"sftp",    UNSET_PORT},
    {"srm",     UNSET_PORT},
    {"root",    UNSET_PORT},
    {"xroot",   UNSET_PORT},
    { NULL,     0xFFFF},
};

/**
 * Lookup a scheme in the list of supported schemes.
 *
 * FUTR: Could also look at configuration so that admins can specify
 * additional supported schemes.
 *
 * @param name Name of the scheme, comparisons are case-insensitive.
 * @return The scheme_t with the passed name or NULL if name is not known.
 */
static const scheme_t *lookup_scheme_by_name(const char *name) {
    if (name != NULL) {
        for (const scheme_t *scheme = schemes; scheme->name != NULL; ++scheme) {
            if (strcasecmp(name, scheme->name) == 0) {
                return scheme;
            }
        }
    }

    return NULL;
}

/**
 * Lookup the default port for a given scheme.
 */
static apr_port_t default_port_of_scheme(const char *name) {
    const scheme_t *scheme = lookup_scheme_by_name(name);
    if (scheme == NULL) {
        return 0;
    }
    return scheme->default_port;
}

/**
 * Checks whether the @a host matches ServerName or any ServerAlias defined
 * for @a s.
 *
 * Helper for matches_request_vhost
 *
 * Almost a verbatim copy of matches_aliases() in HTTPD's vhost.c.
 */
static int matches_server_name_and_aliases(server_rec *s, const char *host) {
    int i;
    apr_array_header_t *names;

    /* match ServerName */
    if (!strcasecmp(host, s->server_hostname)) {
        return 1;
    }

    /* search all the aliases from ServerAlias directive */
    names = s->names;
    if (names) {
        char **name = (char **) names->elts;
        for (i = 0; i < names->nelts; ++i) {
            if (!name[i]) continue;
            if (!strcasecmp(host, name[i]))
                return 1;
        }
    }
    names = s->wild_names;
    if (names) {
        char **name = (char **) names->elts;
        for (i = 0; i < names->nelts; ++i) {
            if (!name[i]) continue;
            if (!ap_strcasecmp_match(host, name[i]))
                return 1;
        }
    }
    return 0;
}

/**
 * Checks if the combination of @a host @a port could possibly be answered by
 * the vhost of @a r.
 *
 * This does not imply that such a request will also be dispatched to the
 * virtual host, because HTTPD evaluates priority between virtual hosts in a
 * more complicated way.
 *
 * Checks addresses and ports in the VirtualHost directive, ServerName
 * directive and ServerAlias directive.
 *
 * Notably missing is a check against the Host header of the request. That's
 * because a client can send any Host header and the request may still be
 * handled by the default/etc. VirtualHost.
 *
 * Note that there is a difference between port, which is an actual property
 * of an TCP connection, and the hostname, which is only used at the client
 * to resolve an IP and at the server for virtual host lookup. There may be
 * problems if the port is changed as part of a reverse proxy set up.
 * FUTR: Consider if a configuration option should be available to specify
 * the current endpoint explicitly.
 *
 * Implementation based on ap_matches_request_vhost but had to be adapted
 * because the original port check is unreasonable.
 */
static int matches_request_vhost(request_rec *r, const char *host, apr_port_t port) {
    int found_port = 0;

    // Search the addresses (host/ip and port) parsed from the VirtualHost directive.
    for (server_addr_rec *sar = r->server->addrs; sar; sar = sar->next) {
        if ((sar->host_port == 0 || port == sar->host_port) && !strcasecmp(host, sar->virthost))
            return 1;

        if (sar->host_port == port)
            found_port = 1;
    }

    // Check server->port, set from ServerName directive
    if (port == r->server->port)
        found_port = 1;

    // Abort if the VirtualHost does not serve the specified port at all
    if (!found_port)
        return 0;

    // Finally, check ServerName along with ServerAlias
    return matches_server_name_and_aliases(r->server, host);
}

static int is_endpoint_remote(request_rec *r, const apr_uri_t *endpoint_uri) {
    // FUTR: Check scheme as well, HTTPD can only handle HTTP and HTTPS (by default)

    if (endpoint_uri->hostname == NULL || endpoint_uri->port == 0) {
        return 0;
    }

    return !matches_request_vhost(r, endpoint_uri->hostname, endpoint_uri->port);
}


/**
 * Writes an error HTTP response including an HTML-wrapped error message.
 *
 * The return value of this function is DONE and can be used as a return value
 * in handler hooks.
 *
 * The response includes the the server's signature (i.e. the hostname and
 * "Apache").
 *
 * Based on mod_dav's dav_error_response() which is unfortunately private.
 */
static int respond_with_error_page(request_rec *r, int status, const char *message) {
    r->status = status;
    r->status_line = ap_get_status_line(status);

    ap_set_content_type(r, "text/html; charset=ISO-8859-1");

    ap_rvputs(
        r,
        DAV_RESPONSE_BODY_1,
        r->status_line,
        DAV_RESPONSE_BODY_2,
        &r->status_line[4],
        DAV_RESPONSE_BODY_3,
        message,
        DAV_RESPONSE_BODY_4,
        ap_psignature("<hr />\n", r),
        DAV_RESPONSE_BODY_5,
        NULL
    );

    return DONE;
}

/**
 * Describes which transfer headers from the TPC request to copy to the request
 * sent to the passive endpoint.
 */
typedef enum {
    DTPC_COPY_HEADERS_NONE,  /**< none, copy no headers */
    DTPC_COPY_HEADERS_AUTHENTICATION,   /**< authentication, copy authentication/credential headers */
    DTPC_COPY_HEADERS_ALL,   /**< all, copy all headers */
} copy_headers_e;

const char *copy_headers_e_to_name(copy_headers_e value) {
    switch (value) {
    case DTPC_COPY_HEADERS_NONE: return "none";
    case DTPC_COPY_HEADERS_AUTHENTICATION: return "authentication";
    case DTPC_COPY_HEADERS_ALL: return "all";
    }
    return "[invalid value]";
}

typedef struct {
    const char *scheme;
    /** bit map of action_e values */
    int allowed_actions;
} scheme_config_t;

/**
 * Represents TPC-related configuration for a specific endpoint.
 * Fully built.
 */
typedef struct {
    const apr_uri_t *endpoint_uri;
    /** bit map of action_e values */
    int allowed_actions;
    copy_headers_e copy_headers;
    const char *checksum_algorithm;
} endpoint_config_t;

typedef enum {
    DTPC_ECK_ALLOWED,  /**< allowed */
    DTPC_ECK_COPY_HEADERS,  /**< copy_headers */
    DTPC_ECK_CHECKSUM_ALGORITHM,  /**< checksum_algorithm */
} endpoint_config_key_e;

typedef struct {
    endpoint_config_key_e key;
    union {
        int int_value;
        copy_headers_e copy_headers_value;
        const char *string_value;
    };
} endpoint_options_stack_option_t;

typedef struct {
    apr_array_header_t *options;
} endpoint_options_stack_t;

static apr_status_t eos_create(apr_pool_t *pool, endpoint_options_stack_t **stack) {
    *stack = apr_pcalloc(pool, sizeof(endpoint_options_stack_t));
    if (*stack == NULL) return APR_ENOMEM;

    (*stack)->options = apr_array_make(pool, 0, sizeof(endpoint_options_stack_option_t));
    if ((*stack)->options == NULL) return APR_ENOMEM;

    return APR_SUCCESS;
}

/**
 * Adds option @a option to the option stack by copying its value shallowly.
 */
static apr_status_t eos_add_option(endpoint_options_stack_t *stack, const endpoint_options_stack_option_t *option) {
    APR_ARRAY_PUSH(stack->options, endpoint_options_stack_option_t) = *option;
    return APR_SUCCESS;
}

static apr_status_t eos_apply_to_config(const endpoint_options_stack_t *stack, endpoint_config_t *config) {
    for (int i = 0; i < stack->options->nelts; ++i) {
        const endpoint_options_stack_option_t *option = &APR_ARRAY_IDX(stack->options, i,
                                                                       endpoint_options_stack_option_t);

        switch (option->key) {
        case DTPC_ECK_ALLOWED:
            config->allowed_actions = option->int_value;
            break;
        case DTPC_ECK_COPY_HEADERS:
            config->copy_headers = option->copy_headers_value;
            break;
        case DTPC_ECK_CHECKSUM_ALGORITHM:
            config->checksum_algorithm = option->string_value;
            break;
        }
    }

    return APR_SUCCESS;
}

typedef struct {
    const apr_uri_t *endpoint_uri;
    const endpoint_options_stack_t *options;
} endpoints_options_map_element_t;

typedef struct {
    // FUTR: Consider switching to apr_skiplist in APR 1.6.0 for O(log n) searches.
    apr_array_header_t *endpoints;
} endpoints_options_map_t;

static apr_status_t esom_create(apr_pool_t *pool, endpoints_options_map_t **map) {
    *map = apr_pcalloc(pool, sizeof(endpoints_options_map_t));
    if (*map == NULL) return APR_ENOMEM;

    (*map)->endpoints = apr_array_make(pool, 0, sizeof(endpoints_options_map_element_t));
    if ((*map)->endpoints == NULL) return APR_ENOMEM;

    return APR_SUCCESS;
}

/**
 * Adds an endpoints to the map by storing the passed in pointers directly.
 */
static apr_status_t esom_add_endpoint(endpoints_options_map_t *map, const apr_uri_t *endpoint_uri,
        const endpoint_options_stack_t *options) {
    APR_ARRAY_PUSH(map->endpoints, endpoints_options_map_element_t) = (endpoints_options_map_element_t) {
        .endpoint_uri = endpoint_uri,
        .options = options,
    };
    return APR_SUCCESS;
}

static const endpoint_options_stack_t *esom_lookup_endpoint(const endpoints_options_map_t *map,
        const apr_uri_t *endpoint_uri) {
    for (int i = 0; i < map->endpoints->nelts; ++i) {
        const endpoints_options_map_element_t *element = &APR_ARRAY_IDX(map->endpoints, i,
                                                                        endpoints_options_map_element_t);

        if (
            element->endpoint_uri->port == endpoint_uri->port &&
            strcmp(element->endpoint_uri->scheme, endpoint_uri->scheme) == 0 &&
            strcmp(element->endpoint_uri->hostname, endpoint_uri->hostname) == 0
        ) {
            return element->options;
        }
    }

    return NULL;
}

/**
 * Represents a TPC request, specifically all transfer-related info.
 *
 * Used for passing TPC info between functions.
 */
typedef struct {
    action_e action;
    const char *local_path;
    const apr_uri_t *remote_uri;
    const apr_uri_t *endpoint_uri;
    const endpoint_config_t *endpoint_config;
    int overwrite_existing;
    const apr_table_t *headers;
    const char *bearer_token;
} tpc_req_t;

typedef enum {
    DTPC_STATUS_UNSET,
    DTPC_STATUS_SUCCESS,
    DTPC_STATUS_FAILED,
} tpc_status_e;

const char *tpc_status_e_to_name(tpc_status_e value) {
    switch (value) {
    case DTPC_STATUS_UNSET: return "unset";
    case DTPC_STATUS_SUCCESS: return "success";
    case DTPC_STATUS_FAILED: return "failed";
    }
    return "[invalid value]";
}

/**
 * State of an ongoing TPC copy operation.
 */
typedef struct {
    /** Request context for the control connection. */
    request_rec *r;
    /** TPC request which resulted in this operation. */
    const tpc_req_t *tpc_req;
    /** Timestamp when the TPC operation was started. */
    apr_time_t start_time;
    /** Timestamp when the TPC copy was started. */
    apr_time_t copy_start_time;
    /** Timestamp when the last Perf Marker was sent to the controlling client. */
    apr_time_t last_perf_marker_time;
    /** Best effort updated number of bytes transferred so far. */
    size_t bytes_transferred;
    /** Whether a perf-marker-stream response has been started. */
    int started_perf_marker_stream;
} tpc_state_t;

typedef struct {
    action_e action;
    const char *local_path;
    const apr_uri_t *remote_uri;
    const apr_uri_t *endpoint_uri;
    apr_size_t bytes_transferred;
    apr_time_t start_time;
    apr_time_t end_time;
    tpc_status_e status;
    apr_status_t error_status;
} tpc_req_summary_t;

static void fill_summary_from_success(const tpc_req_t *req, const tpc_state_t *state, tpc_req_summary_t *summary) {
    *summary = (tpc_req_summary_t) {
        .action = req->action,
        .local_path = req->local_path,
        .remote_uri = req->remote_uri,
        .endpoint_uri = req->endpoint_uri,
        .bytes_transferred = state->bytes_transferred,
        .start_time = state->start_time,
        .end_time = apr_time_now(),
        .status = DTPC_STATUS_SUCCESS,
        .error_status = APR_SUCCESS,
    };
}

static void fill_summary_from_failed(const tpc_req_t *req, apr_time_t start_time, apr_status_t error_status,
        tpc_req_summary_t *summary) {
    *summary = (tpc_req_summary_t) {
        .action = req->action,
        .local_path = req->local_path,
        .remote_uri = req->remote_uri,
        .endpoint_uri = req->endpoint_uri,
        .bytes_transferred = 0,
        .start_time = start_time,
        .end_time = apr_time_now(),
        .status = DTPC_STATUS_FAILED,
        .error_status = error_status,
    };
}

// HTTPD configuration structs.

typedef struct {
} module_global_config_t;

typedef struct {
} module_server_config_t;

typedef struct {
    apr_array_header_t *allowed_schemes;
    endpoint_options_stack_t *endpoint_defaults;
    endpoints_options_map_t *endpoints;
} module_dir_config_t;

static module_global_config_t global_config;

typedef struct {
    tpc_req_summary_t *summary;
} module_request_config_t;

static apr_status_t initiate_perf_marker_stream_response(request_rec *r) {
    r->status = 202;
    r->status_line = ap_get_status_line(202);

    ap_set_content_type(r, "text/perf-marker-stream; charset=UTF-8");
    if (ap_rflush(r) != 0) {
        return APR_EGENERAL;
    }
    return APR_SUCCESS;
}

static void perform_tpc_request_event_cb(const gfalt_event_t e, gpointer user_data) {
    tpc_state_t *tpc_req_state = (tpc_state_t *) user_data;

    const char *side_str;
    switch (e->side) {
    case GFAL_EVENT_SOURCE:
        side_str = "source";
        break;
    case GFAL_EVENT_DESTINATION:
        side_str = "destination";
        break;
    case GFAL_EVENT_NONE:
        side_str = "none";
        break;
    default:
        side_str = "unknown";
        break;
    }
    const char *stage_str = g_quark_to_string(e->stage);
    const char *domain_str = g_quark_to_string(e->domain);

    ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, tpc_req_state->r,
                  "TPC state change: timestamp=%ld, side=%s, stage=%s, domain=%s, description=%s",
                  e->timestamp, side_str, stage_str, domain_str, e->description);

    if (e->stage == GFAL_EVENT_TRANSFER_ENTER && !tpc_req_state->started_perf_marker_stream) {
        apr_status_t status = initiate_perf_marker_stream_response(tpc_req_state->r);
        tpc_req_state->started_perf_marker_stream = 1;
        if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_ERR, status, tpc_req_state->r,
                          "Error while initiating perf-marker-stream (but continuing): %s",
                          dtpc_strerror_pool(tpc_req_state->r->pool, status));
        }
    }
}

static void perform_tpc_request_monitor_cb(gfalt_transfer_status_t status, const char *src, const char *dst,
        gpointer user_data) {
    tpc_state_t *tpc_req_state = (tpc_state_t *) user_data;

    GError* error = NULL;

    apr_time_t now = apr_time_now();

    int status_code = gfalt_copy_get_status(status, &error);
    ap_assert(error == NULL);
    size_t avg_baudrate = gfalt_copy_get_average_baudrate(status, &error);
    ap_assert(error == NULL);
    size_t inst_baudrate = gfalt_copy_get_instant_baudrate(status, &error);
    ap_assert(error == NULL);
    size_t bytes_transferred = gfalt_copy_get_bytes_transfered(status, &error);
    ap_assert(error == NULL);
    time_t elapsed_seconds = gfalt_copy_get_elapsed_time(status, &error);
    ap_assert(error == NULL);

    tpc_req_state->bytes_transferred = bytes_transferred;

    ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, tpc_req_state->r, "TPC monitor status: status_code=%d, "
                  "avg_baudrate=%lu, inst_baudrate=%lu, bytes_transferred=%lu, elapsed_seconds=%lu",
                  status_code, avg_baudrate, inst_baudrate, bytes_transferred, elapsed_seconds);

    // Only send Perf Marker if the last was sent > 3 s ago
    if (tpc_req_state->started_perf_marker_stream &&
            now > tpc_req_state->last_perf_marker_time + apr_time_from_sec(3)) {
        tpc_req_state->last_perf_marker_time = now;

        ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, tpc_req_state->r, "Sending Perf Marker to controlling client");

        ap_rprintf(tpc_req_state->r,
                  "Perf Marker\n"
                  "Timestamp: %lu\n"
                  "Stripe Index: 0\n"
                  "Stripe Start Time: %lu\n"
                  "Stripe Transfer Time: %lu\n"
                  "Stripe Bytes Transferred: %lu\n"
                  "Total Stripe Count: 1\n"
                  "End\n",
                  apr_time_sec(now), apr_time_sec(tpc_req_state->copy_start_time), elapsed_seconds, bytes_transferred);
        ap_rflush(tpc_req_state->r);
    }
}

static apr_status_t headers_table_to_headers_list(apr_pool_t *pool, const apr_table_t *headers,
        const char * const **list, int *length) {
    const apr_array_header_t *entries = apr_table_elts(headers);

    const char **build_list = apr_pcalloc(pool, sizeof(char *)*(entries->nelts+1));
    if (build_list == NULL) {
        return APR_ENOMEM;
    }

    for (int i = 0; i < entries->nelts; ++i) {
        apr_table_entry_t entry = APR_ARRAY_IDX(entries, i, apr_table_entry_t);

        char *line = apr_pstrcat(pool, entry.key, ":", entry.val, NULL);
        if (line == NULL) {
            return APR_ENOMEM;
        }

        build_list[i] = line;
    }

    build_list[entries->nelts] = NULL;

    *list = build_list;
    *length = entries->nelts + 1; // TODO: is the +1 correct?

    return APR_SUCCESS;
}

static void perform_tpc_request(request_rec *r, const tpc_req_t *tpc_req) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Starting TPC procedure");

    // Extract summary from the module config in the request.
    // This approach can be improved upon as the summary was just created by the caller to perform_tpc_request.
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    ap_assert(req_config != NULL);
    tpc_req_summary_t *summary = req_config->summary;
    ap_assert(summary != NULL);

    // GFAL2's file plugin does not do any de-escaping of the file path
    const char *local_uri_str = apr_pstrcat(r->pool, "file://", tpc_req->local_path, NULL);
    ap_assert(local_uri_str != NULL);
    const char *remote_uri_str = apr_uri_unparse(r->pool, tpc_req->remote_uri, 0);
    ap_assert(remote_uri_str != NULL);

    const char *source_uri, *destination_uri;
    if (tpc_req->action == DTPC_ACTION_PULL) {
        source_uri = remote_uri_str;
        destination_uri = local_uri_str;
    } else {
        source_uri = local_uri_str;
        destination_uri = remote_uri_str;
    }

    // Perform checks on source and destination.

    // TODO: Check whether local file is suitable for the COPY operation, e.g. the file may not
    //       exist, this module has to generate the 404.
    //       See dav_method_copymove, which performs dav_get_resource() on source and
    //       dav_lookup_uri (a wrapper around ap_sub_req_method_uri) on the destination.
    //       r->finfo can also be inspected, which contains the result of an earlier stat call.
    //       This may not be enough to generate a 403 when the read permission of the file is
    //       missing for the current user. If a parent directory is not accessible because of
    //       permissions a 403 has already been generated earlier.
    //
    //       The difficulty of responding with proper HTTP status codes is because once a
    //       perf-marker-stream has been started it's no longer possible to respond with a normal
    //       HTTP status. The perf-marker-stream is started as soon as GFAL2 reached stage
    //       TRANSFER:ENTER.
    //       Some checks, particularly on destination, are performed earlier by GFAL2 during stage
    //       PREPARE:*. For example, the existence of the destination is checked to trigger
    //       overwriting logic. Any errors during this phase are handled by the error handling
    //       block following gfalt_copy_file.
    if (tpc_req->action == DTPC_ACTION_PULL) {
        // No implementation yet
    } else {
        if (r->finfo.filetype == APR_NOFILE) {
            respond_with_error_page(r, HTTP_NOT_FOUND, "Local source URL does not exist.");
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_ENOENT, r,
                          "TPC transfer of \"%s\" -> \"%s\" failed (during preparation): Local source URL "
                          "does not exist", source_uri, destination_uri);
            fill_summary_from_failed(tpc_req, r->request_time, APR_ENOENT, summary);
            return;
        } else if (r->finfo.filetype != APR_REG && r->finfo.filetype != APR_LNK) {
            respond_with_error_page(r, HTTP_BAD_REQUEST, "Local source URL is not a regular file.");
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, APR_EINVAL, r,
                          "TPC transfer of \"%s\" -> \"%s\" failed (during preparation): Local source URL "
                          "is not a regular file", source_uri, destination_uri);
            fill_summary_from_failed(tpc_req, r->request_time, APR_EINVAL, summary);
            return;
        }
    }

    // TODO: Analogously to the local file, a stat on the remote file would allow to generate a
    //       proper error.
    // TODO: An early stat on the remote file also reveals whether a GFAL2 plugin is available
    //       for handling the access.

    GError* error = NULL;

    gfal2_context_t context = gfal2_context_new(&error);
    ap_assert(error == NULL);

    if (tpc_req->bearer_token != NULL) {
        gfal2_set_opt_string(context, "BEARER", "TOKEN", tpc_req->bearer_token, &error);
        ap_assert(error == NULL);
    }

    if (tpc_req->headers != NULL) {
        const char * const *list;
        int length;
        apr_status_t status = headers_table_to_headers_list(r->pool, tpc_req->headers, &list, &length);
        ap_assert(status == APR_SUCCESS);
        gfal2_set_opt_string_list(context, "HTTP PLUGIN", "HEADERS", list, length, &error);
        ap_assert(error == NULL);
    }

    gfal2_set_user_agent(context, "mod_dav_tpc", DAV_TPC_VERSION, &error);
    ap_assert(error == NULL);

    gfalt_params_t params = gfalt_params_handle_new(&error);
    ap_assert(error == NULL);

    // TODO: Timeout...
    //       It seems that the timeout is basically ignored by Davix until the PUT has completed.
    //       There may be another way to set a timeout, look at GFAL2 http plugin again.
    // apr_time_t seconds_timeout = 60;
    // gfalt_set_timeout(params, seconds_timeout, &error);
    // ap_assert(error == NULL);
    // There is also (which is the same as set_operation_timeout):
    // gfal2_set_opt_integer(handle, "HTTP PLUGIN", HTTP_CONFIG_OP_TIMEOUT, timeout, NULL);

    if (tpc_req->overwrite_existing)
        gfalt_set_replace_existing_file(params, TRUE, &error);
    else
        gfalt_set_replace_existing_file(params, FALSE, &error);
    ap_assert(error == NULL);

    if (tpc_req->endpoint_config->checksum_algorithm != NULL)
        gfalt_set_checksum(params, GFALT_CHECKSUM_BOTH, tpc_req->endpoint_config->checksum_algorithm, NULL, &error);
    else
        gfalt_set_checksum(params, GFALT_CHECKSUM_NONE, NULL, NULL, &error);
    ap_assert(error == NULL);

    // Only use a single stream for transferring
    // FUTR: Consider including nbstreams as an option in endpoint config
    gfalt_set_nbstreams(params, 1, &error);
    ap_assert(error == NULL);

    tpc_state_t tpc_req_state = {
        .r = r,
        .tpc_req = tpc_req,
        .start_time = r->request_time,
        .copy_start_time = apr_time_now(),
        .last_perf_marker_time = 0,
        .bytes_transferred = 0,
        .started_perf_marker_stream = 0,
    };

    gfalt_add_event_callback(params, perform_tpc_request_event_cb, &tpc_req_state, NULL, &error);
    ap_assert(error == NULL);
    gfalt_add_monitor_callback(params, perform_tpc_request_monitor_cb, &tpc_req_state, NULL, &error);
    ap_assert(error == NULL);

    // When the destination is local and the source is HTTP, the
    // generic "localcopy" algorithm performs the copy operation.
    // When the destination is HTTP and the source is local, the HTTP plugin
    // performs the copy operation. It will always attempt to do a "stream"
    // copy because the source is not a HTTP URI. However, the query
    // parameter "copy_mode" of the destination URI will be used to change
    // some settings related to TPC if passed. These changes have no effect
    // because TPC is not possible.
    gfalt_copy_file(context, params, source_uri, destination_uri, &error);
    if (error) {
        int log_level = APLOG_ERR;

        if (tpc_req_state.started_perf_marker_stream) {
            ap_rvputs(r, "Failed: ", error->message, "\nEnd\n", NULL);
        } else {
            // Handle select error cases, responding with proper HTTP status
            // FUTR: Handle PUT returning 409 by answering with 409 (HTTP_CONFLICT)
            //       But a 409 is not yet properly indicated by GFAL2 (which returns EEXIST)
            if (!tpc_req->overwrite_existing && error->code == EEXIST) {
                respond_with_error_page(r, HTTP_PRECONDITION_FAILED, "Destination exists but request specified "
                                        "no overwriting.");
                log_level = APLOG_DEBUG;
            } else if (error->code == EPERM) {
                const char *message = apr_psprintf(r->pool, "Access forbidden: %s", error->message);
                ap_assert(message != NULL);
                respond_with_error_page(r, HTTP_FORBIDDEN, message);
                log_level = APLOG_WARNING;
            } else if (error->code == ETIMEDOUT) {
                respond_with_error_page(r, HTTP_GATEWAY_TIME_OUT, "Network connection to remote endpoint timed out.");
                log_level = APLOG_WARNING;
            } else if (error->code == EHOSTUNREACH) {
                respond_with_error_page(r, HTTP_BAD_GATEWAY, "Remote endpoint unreachable.");
                log_level = APLOG_WARNING;
            } else {
                const char *message = apr_psprintf(r->pool, "Error while performing TPC: %s", error->message);
                ap_assert(message != NULL);
                respond_with_error_page(r, HTTP_INTERNAL_SERVER_ERROR, message);
            }
        }

        ap_log_rerror(APLOG_MARK, log_level, APR_FROM_OS_ERROR(error->code), r,
                      "TPC transfer of \"%s\" -> \"%s\" failed (during GFAL2 copy): %s", source_uri, destination_uri,
                      error->message);

        fill_summary_from_failed(tpc_req, r->request_time, APR_FROM_OS_ERROR(error->code), summary);

        g_clear_error(&error);

        gfalt_params_handle_delete(params, &error);
        ap_assert(error == NULL);
        gfal2_context_free(context);

        return;
    }

    // FUTR: Aborting a transfer (gfal2_cancel) and cleaning up
    //       How to detect aborted requests (connection closed by user) within HTTPD?
    //       mod_log_config has %X which contains info about aborted connections before response finished.

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "TPC transfer of \"%s\" -> \"%s\" succeeded, "
                  "approx %lu bytes transferred", source_uri, destination_uri, tpc_req_state.bytes_transferred);

    fill_summary_from_success(tpc_req, &tpc_req_state, summary);

    gfalt_params_handle_delete(params, &error);
    ap_assert(error == NULL);
    gfal2_context_free(context);

    if (!tpc_req_state.started_perf_marker_stream) {
        apr_status_t status = initiate_perf_marker_stream_response(r);
        tpc_req_state.started_perf_marker_stream = 1;
        if (status != APR_SUCCESS) {
            ap_log_rerror(APLOG_MARK, APLOG_ERR, status, r,
                          "Error while late-initiating perf-marker-stream (but continuing): %s",
                          dtpc_strerror_pool(r->pool, status));
        }
    }

    ap_rvputs(r, "Success\nEnd\n", NULL);
    ap_rflush(r);
}

// For debugging only
static void print_uri(request_rec *r, const apr_uri_t *uri) {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
        "apr_uri_t{scheme=%s, hostinfo=%s, user=%s, password=%s, hostname=%s, port_str=%s, path=%s, query=%s,"
        "fragment=%s, port=%d, is_initialized=%d, dns_looked_up=%d, dns_resolved=%d",
        uri->scheme, uri->hostinfo, uri->user, uri->password, uri->hostname, uri->port_str, uri->path, uri->query,
        uri->fragment, uri->port, uri->is_initialized, uri->dns_looked_up, uri->dns_resolved
    );
}

// For debugging only
static void print_eos(request_rec *r, const endpoint_options_stack_t *stack) {
    for (int i = 0; i < stack->options->nelts; ++i) {
        const endpoint_options_stack_option_t *option = &APR_ARRAY_IDX(stack->options, i,
                                                                       endpoint_options_stack_option_t);

        switch (option->key) {
        case DTPC_ECK_ALLOWED:
            ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, r, "option: allowed, value: %d", option->int_value);
            break;
        case DTPC_ECK_COPY_HEADERS:
            ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, r, "option: copy_headers, value: %s",
                          copy_headers_e_to_name(option->copy_headers_value));
            break;
        case DTPC_ECK_CHECKSUM_ALGORITHM:
            ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, r, "option: checksum_algorithm, value: %s",
                          option->string_value);
            break;
        }
    }
}

static int is_endpoint_uri(const apr_uri_t *uri) {
    if (!uri->is_initialized)
        return 0;

    if (uri->scheme == NULL)
        return 0;
    for (const char *c = uri->scheme; *c != '\x0'; ++c) {
        if (!apr_islower(*c))
            return 0;
    }

    if (uri->hostname == NULL)
        return 0;
    for (const char *c = uri->hostname; *c != '\x0'; ++c) {
        if (!apr_islower(*c))
            return 0;
    }

    if (uri->port == 0)
        return 0;

    if (uri->user != NULL || uri->password != NULL || uri->path != NULL || uri->query != NULL || uri->fragment != NULL)
        return 0;

    return 1;
}

/**
 * Formats endpoint_uri @a uri as a string.
 *
 * It is the caller's responsibility to ensure is_endpoint_uri(uri) == 1.
 *
 * Implementation based on apr_uri_unparse().
 */
static char *endpoint_uri_to_str(apr_pool_t *p, const apr_uri_t *uri) {
    const char *pre_hostname = "", *post_hostname = "";

    if (strchr(uri->hostname, ':')) {
        // Hostname is an IPv6 literal
        pre_hostname = "[";
        post_hostname = "]";
    }

    // The port is always added to avoid ambiguity
    const char *port_str = uri->port_str;
    if (port_str == NULL) {
        port_str = apr_itoa(p, uri->port);
        ap_assert(port_str != NULL);
    }

    return apr_pstrcat(p, uri->scheme, "://", pre_hostname, uri->hostname, post_hostname, ":", port_str, NULL);
}

static apr_status_t extract_endpoint_uri_from_uri(apr_pool_t *p, const apr_uri_t *uri, apr_uri_t *result) {
    apr_uri_t endpoint_uri = {0};

    if (uri->scheme == NULL) {
        return DTPC_INVALID_URI;
    }
    endpoint_uri.scheme = apr_pstrdup(p, uri->scheme);
    ap_assert(endpoint_uri.scheme != NULL);
    ap_str_tolower(endpoint_uri.scheme);

    if (uri->hostname == NULL) {
        return DTPC_INVALID_URI;
    }
    endpoint_uri.hostname = apr_pstrdup(p, uri->hostname);
    ap_assert(endpoint_uri.hostname != NULL);
    ap_str_tolower(endpoint_uri.hostname);

    if (uri->port != 0) {
        endpoint_uri.port = uri->port;
        endpoint_uri.port_str = uri->port_str;
    } else {
        endpoint_uri.port = default_port_of_scheme(endpoint_uri.scheme);
        if (endpoint_uri.port == 0) {
            return DTPC_UNDETERMINABLE_PORT;
        }
        endpoint_uri.port_str = apr_itoa(p, endpoint_uri.port);
        ap_assert(endpoint_uri.port_str != NULL);
    }

    endpoint_uri.is_initialized = 1;

    *result = endpoint_uri;

    return APR_SUCCESS;
}

/**
 * Parses basics of a TPC request from header fields.
 *
 * This consists of the TPC action, local_path, remote_uri and the derived
 * endpoint_uri.
 * The @a tpc_req is set to its zero-value at the beginning of the function
 * before returning any error.
 *
 * The message is suitable for presenting to users/admins.
 */
static enr_status_t parse_tpc_basics_from_header(request_rec *r, tpc_req_t *tpc_req) {
    apr_status_t status;

    const char *source = apr_table_get(r->headers_in, "Source");
    const char *destination = apr_table_get(r->headers_in, "Destination");
    const char *remote_uri_str = NULL;

    *tpc_req = (tpc_req_t) {0};

    if (source != NULL && destination != NULL) {
        return (enr_status_t) {
            .status = DTPC_MALFORMED_HTTP_REQUEST,
            .message = "Both Source and Destination header fields are present in COPY request",
        };
    }

    if (destination != NULL) {
        remote_uri_str = destination;
        tpc_req->action = DTPC_ACTION_PUSH;
    } else if (source != NULL) {
        remote_uri_str = source;
        tpc_req->action = DTPC_ACTION_PULL;
    } else {
        // mod_dav may still be able to handle this, so declare this case as
        // an invalid URI instead of a malformed request
        return (enr_status_t) {
            .status = DTPC_INVALID_URI,
            .message = "Neither Source nor Destination header fields are present in COPY request",
        };
    }

    // TODO: Does this have to be checked for NULL, 0-length, etc.?
    tpc_req->local_path = r->filename;
    // TODO: Look at r->path_info
    //       Apparently this has to be inspected as well (see repos.c) as it
    //       indicates that not all data in the path of the request was stored
    //       in r->filename

    apr_uri_t *remote_uri = apr_pcalloc(r->pool, sizeof(apr_uri_t));
    if (remote_uri == NULL) return (enr_status_t) { .status = APR_ENOMEM, .message = NULL };
    apr_uri_t *endpoint_uri = apr_pcalloc(r->pool, sizeof(apr_uri_t));
    if (endpoint_uri == NULL) return (enr_status_t) { .status = APR_ENOMEM, .message = NULL };

    status = apr_uri_parse(r->pool, remote_uri_str, remote_uri);
    if (status != APR_SUCCESS) {
        return (enr_status_t) {
            .status = status,
            .message = "Parsing of remote URI (Destination/Source header field) failed",
        };
    }
    ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, r, "Extracted remote URI %s for %s COPY from headers",
                  remote_uri_str, action_e_to_name(tpc_req->action));

    if (remote_uri->scheme == NULL) {
        return (enr_status_t) {
            .status = DTPC_INVALID_URI,
            .message = "Remote URI (Destination/Source header field) is missing a scheme",
        };
    }
    if (remote_uri->hostname == NULL) {
        return (enr_status_t) {
            .status = DTPC_INVALID_URI,
            .message = "Remote URI (Destination/Source header field) is missing a hostname",
        };
    }
    if (remote_uri->fragment != NULL) {
        return (enr_status_t) {
            .status = DTPC_INVALID_URI,
            .message = "Remote URI (Destination/Source header field) contains a fragment component (#), "
                       "this is likely unintended",
        };
    }

    const scheme_t *scheme = lookup_scheme_by_name(remote_uri->scheme);
    if (scheme == NULL) {
        return (enr_status_t) {
            .status = DTPC_UNKNOWN_SCHEME,
            .message = "Remote URI (Destination/Source header field) uses an unsupported scheme",
        };
    }
    // Replace scheme with well-known scheme string pointer for faster compares
    // Need to discard const qualifier
    remote_uri->scheme = (char *) scheme->name;

    status = extract_endpoint_uri_from_uri(r->pool, remote_uri, endpoint_uri);
    if (status != APR_SUCCESS) {
        return (enr_status_t) {
            .status = status,
            .message = "Failed to extract endpoint info from remote URI (Destination/Source header field)",
        };
    }
    // Replace scheme with well-known scheme string pointer for faster compares
    // Need to discard const qualifier
    endpoint_uri->scheme = (char *) scheme->name;

    // Set the (default) port explicitly from the endpoint URI
    // However, apr_uri_unparse may drop the default port during conversion
    remote_uri->port = endpoint_uri->port;
    remote_uri->port_str = endpoint_uri->port_str;

    if (APLOGrdebug(r)) {
        ap_log_rerror(APLOG_MARK, APLOG_TRACE1, 0, r,
                      "Extracted endpoint URI %s for %s COPY from remote URI %s",
                      endpoint_uri_to_str(r->pool, endpoint_uri), action_e_to_name(tpc_req->action),
                      remote_uri_str);
    }

    tpc_req->remote_uri = remote_uri;
    tpc_req->endpoint_uri = endpoint_uri;

    return enr_status_success;
}

static int str_has_case_prefix(const char *str, const char *prefix) {
    return strncasecmp(prefix, str, strlen(prefix)) == 0;
}

static int scan_table_copy_transfer_headers(void *rec, const char *key, const char *value) {
    apr_table_t *headers = rec;

    if (str_has_case_prefix(key, "TransferHeader")) {
        const char *key_clean = key + 14; // Remove prefix
        if (strcasecmp(key_clean, "Authorization") == 0) {
            // Skip
        } else {
            apr_table_add(headers, key_clean, value);
        }
    }

    return 1;
}

/**
 * Parses extended info of a TPC request from header fields with a known
 * endpoint config.
 *
 * This consists of the Overwrite header along with any TransferHeader headers.
 * The endpoint_config of the @a tpc_req is respected and only relevant fields
 * of @a tpc_req are set. It is expected that before calling this function
 * these fields have a suitable zero value, e.g. as initialised by
 * parse_tpc_basics_from_header.
 *
 * The message is suitable for presenting to users/admins.
 */
static enr_status_t parse_tpc_extended_from_header(request_rec *r, tpc_req_t *tpc_req) {
    tpc_req->overwrite_existing = 1;
    const char *overwrite_str = apr_table_get(r->headers_in, "Overwrite");
    if (overwrite_str != NULL) {
        if (strcmp(overwrite_str, "T") == 0) {
            tpc_req->overwrite_existing = 1;
        } else if (strcmp(overwrite_str, "F") == 0) {
            tpc_req->overwrite_existing = 0;
        } else {
            return (enr_status_t) {
                .status = DTPC_MALFORMED_HTTP_REQUEST,
                .message = "Invalid value in Overwrite header",
            };
        }
    }

    switch (tpc_req->endpoint_config->copy_headers) {
    case DTPC_COPY_HEADERS_ALL:
        ; // C does not allow declaration right after case
        apr_table_t *headers = apr_table_make(r->pool, 0);

        apr_table_do(scan_table_copy_transfer_headers, headers, r->headers_in, NULL);

        // Join multiple values for the same key by concatenating with ", "
        apr_table_compress(headers, APR_OVERLAP_TABLES_MERGE);

        tpc_req->headers = headers;

        // fallthrough...
    case DTPC_COPY_HEADERS_AUTHENTICATION:
        ; // C does not allow declaration right after case
        const char *value = apr_table_get(r->headers_in, "TransferHeaderAuthorization");
        if (value != NULL) {
            if (str_has_case_prefix(value, "Bearer ")) {
                tpc_req->bearer_token = value+7;
            } else {
                ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
                              "Ignoring Authorization transfer header with unknown scheme: %s", value);
            }
        }

        // fallthrough...
    case DTPC_COPY_HEADERS_NONE:
        break;
    }

    return enr_status_success;
}

static int is_tpc_scheme_action_allowed(request_rec *r, const tpc_req_t *tpc_req) {
    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &dav_tpc_module);

    if (dir_config->allowed_schemes == NULL) {
        return 0;
    }

    for (int i = 0; i < dir_config->allowed_schemes->nelts; ++i) {
        scheme_config_t *permissions = APR_ARRAY_IDX(dir_config->allowed_schemes, i, scheme_config_t *);
        if (strcasecmp(tpc_req->remote_uri->scheme, permissions->scheme) == 0) {
            return (tpc_req->action & permissions->allowed_actions) > 0;
        }
    }

    return 0;
}

static apr_status_t build_endpoint_config(request_rec *r, const apr_uri_t *endpoint_uri, endpoint_config_t *config) {
    *config = (endpoint_config_t) {0};
    config->endpoint_uri = endpoint_uri;

    // Apply zero-value defaults explicitly
    config->allowed_actions = 0;
    config->copy_headers = DTPC_COPY_HEADERS_NONE;
    config->checksum_algorithm = NULL;

    module_dir_config_t *dir_config = (module_dir_config_t *) ap_get_module_config(r->per_dir_config, &dav_tpc_module);

    if (dir_config->endpoint_defaults != NULL) {
        eos_apply_to_config(dir_config->endpoint_defaults, config);
    }

    if (dir_config->endpoints != NULL) {
        const endpoint_options_stack_t *options = esom_lookup_endpoint(dir_config->endpoints, endpoint_uri);
        if (options != NULL) {
            eos_apply_to_config(options, config);
        }
    }

    return APR_SUCCESS;
}

static int is_tpc_req_to_endpoint_allowed(const tpc_req_t *tpc_req) {
    return (tpc_req->action & tpc_req->endpoint_config->allowed_actions) > 0;
}

/**
 * Steal processing from the mod_dav module if a TPC attempt is detected.
 *
 * An alternative would be to already detect these cases during fixups
 * (after mod_dav ran) and replace the handler there.
 */
static int handle_tpc_decline_otherwise(request_rec *r) {
    // Only look at requests which are meant to be handled by mod_dav
    if (strcmp(r->handler, DAV_HANDLER_NAME) != 0)
        return DECLINED;

    // TODO: Check if the filesystem DAV provider is active
    //       (difficult because this is hidden inside mod_dav's module config)

    // From mod_dav: Reject requests with a "fragment" in the path.
    // These requests are likely already rejected by HTTPD's core.
    if (r->parsed_uri.fragment != NULL) {
        // Let mod_dav handle this case, generating an error
        return DECLINED;
    }

    // This function only ever takes action for COPY requests
    if (r->method_number != M_COPY) {
        return DECLINED;
    }

    apr_status_t status;
    enr_status_t enr_status;
    tpc_req_t tpc_req;
    endpoint_config_t endpoint_config;

    enr_status = parse_tpc_basics_from_header(r, &tpc_req);
    if (enr_status.status != APR_SUCCESS) {
        // FUTR: APR_ENOMEM and other severe server-centric errors should lead to a 500
        //       That requires choosing a range of the error space, e.g. only CANONERR or anything
        //       which is outside this module's error space.
        if (enr_status.status == DTPC_MALFORMED_HTTP_REQUEST) {
            // The request is malformed according to HTTP spec, so return an error immediately
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Malformed HTTP COPY request: %s: %s", enr_status.message,
                          dtpc_strerror_pool(r->pool, enr_status.status));
            char *message = apr_pstrcat(r->pool, "The request was invalid: ", enr_status.message, NULL);
            return respond_with_error_page(r, HTTP_BAD_REQUEST, message);
        } else if (enr_status.status == DTPC_UNKNOWN_SCHEME) {
            // Early check for supported URI schemes, mod_dav only supports HTTP and HTTPS,
            // so return an error immediately
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Error while parsing TPC headers: %s: %s", enr_status.message,
                          dtpc_strerror_pool(r->pool, enr_status.status));
            return respond_with_error_page(r, HTTP_NOT_IMPLEMENTED, enr_status.message);
        } else if (tpc_req.action == DTPC_ACTION_PULL) {
            // mod_dav can only COPY with push semantic, so return an error immediately
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Error while parsing TPC headers: %s: %s", enr_status.message,
                          dtpc_strerror_pool(r->pool, enr_status.status));
            char *message = apr_pstrcat(r->pool, "The request was invalid: ", enr_status.message, NULL);
            return respond_with_error_page(r, HTTP_BAD_REQUEST, message);
        } else {
            // mod_dav may still decide this COPY is valid
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r, "Error while parsing TPC headers, handing over to mod_dav: "
                          "%s: %s", enr_status.message, dtpc_strerror_pool(r->pool, enr_status.status));
            return DECLINED;
        }
    }

    if (!is_endpoint_remote(r, tpc_req.endpoint_uri)) {
        if (tpc_req.action == DTPC_ACTION_PULL) {
            // mod_dav can only COPY with push semantic, so return an error immediately
            const char *endpoint_uri_str = endpoint_uri_to_str(r->pool, tpc_req.endpoint_uri);
            ap_assert(endpoint_uri_str != NULL);
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r, "Received COPY with TPC pull but local endpoint as "
                          "destination: %s", endpoint_uri_str);
            return respond_with_error_page(r, HTTP_BAD_REQUEST, "The request was invalid: COPY with Source on the same "
                                           "endpoint");
        } else {
            // mod_dav may handle this local COPY
            ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Received COPY with local endpoint, handing over to mod_dav");
            return DECLINED;
        }
    }

    if (APLOGrdebug(r)) {
        const char *endpoint_uri_str = endpoint_uri_to_str(r->pool, tpc_req.endpoint_uri);
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Received COPY with URI on remote endpoint %s, "
                      "taking over TPC request", endpoint_uri_str);
    }

    // Create and store a summary for later retrieval when printing logs.
    module_request_config_t *req_config = apr_pcalloc(r->pool, sizeof(module_request_config_t));
    ap_assert(req_config != NULL);
    ap_set_module_config(r->request_config, &dav_tpc_module, req_config);
    tpc_req_summary_t *summary = apr_pcalloc(r->pool, sizeof(tpc_req_summary_t));
    ap_assert(summary != NULL);
    req_config->summary = summary;

    if (!is_tpc_scheme_action_allowed(r, &tpc_req)) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
                      "Rejecting TPC request as %s operation via scheme %s is not allowed by config",
                      action_e_to_name(tpc_req.action), tpc_req.remote_uri->scheme);
        fill_summary_from_failed(&tpc_req, r->request_time, APR_EACCES, summary);
        // 405 response is required by FTS3/GFAL2 to trigger fallback to streamed copy
        return respond_with_error_page(r, HTTP_METHOD_NOT_ALLOWED, apr_psprintf(r->pool, "Rejecting TPC request "
                                       "(COPY to/from remote URI) as %s operation via scheme %s is not allowed",
                                       action_e_to_name(tpc_req.action), tpc_req.remote_uri->scheme));
    }

    status = build_endpoint_config(r, tpc_req.endpoint_uri, &endpoint_config);
    if (status != APR_SUCCESS) {
        ap_log_rerror(APLOG_MARK, APLOG_ERR, status, r, "Error while building endpoint config for endpoint %s: %s",
                      endpoint_uri_to_str(r->pool, tpc_req.endpoint_uri), dtpc_strerror_pool(r->pool, status));
        fill_summary_from_failed(&tpc_req, r->request_time, status, summary);
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    tpc_req.endpoint_config = &endpoint_config;

    if (!is_tpc_req_to_endpoint_allowed(&tpc_req)) {
        ap_log_rerror(APLOG_MARK, APLOG_WARNING, 0, r,
                      "Rejecting TPC request as %s operation on endpoint %s is not allowed by config",
                      action_e_to_name(tpc_req.action), endpoint_uri_to_str(r->pool, tpc_req.endpoint_uri));
        fill_summary_from_failed(&tpc_req, r->request_time, APR_EACCES, summary);
        // 405 response is required by FTS3/GFAL2 to trigger fallback to streamed copy
        return respond_with_error_page(r, HTTP_METHOD_NOT_ALLOWED, apr_psprintf(r->pool, "Rejecting TPC request "
                                       "(COPY to/from remote URI) as %s operation on endpoint %s is not allowed",
                                       action_e_to_name(tpc_req.action),
                                       endpoint_uri_to_str(r->pool, tpc_req.endpoint_uri)));
    }

    enr_status = parse_tpc_extended_from_header(r, &tpc_req);
    if (enr_status.status != APR_SUCCESS) {
        // We have already evaluated that the request is a TPC request, so no handing over to mod_dav.
        if (enr_status.status == DTPC_MALFORMED_HTTP_REQUEST) {
            // The request is malformed according to HTTP spec, so return an error immediately
            ap_log_rerror(APLOG_MARK, APLOG_ERR, enr_status.status, r, "Malformed TPC request: %s: %s",
                          enr_status.message, dtpc_strerror_pool(r->pool, enr_status.status));
            fill_summary_from_failed(&tpc_req, r->request_time, enr_status.status, summary);
            char *message = apr_pstrcat(r->pool, "The request was invalid: ", enr_status.message, NULL);
            return respond_with_error_page(r, HTTP_BAD_REQUEST, message);
        } else {
            ap_log_rerror(APLOG_MARK, APLOG_WARNING, enr_status.status, r, "Error while parsing TPC request headers: "
                          "%s: %s", enr_status.message, dtpc_strerror_pool(r->pool, enr_status.status));
            fill_summary_from_failed(&tpc_req, r->request_time, enr_status.status, summary);
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }

    // The function does its own error handling including sending responses
    perform_tpc_request(r, &tpc_req);

    return OK;
}

static void *create_server_config(apr_pool_t *pool, server_rec *s) {
    module_server_config_t *config = (module_server_config_t *) apr_pcalloc(pool, sizeof(module_server_config_t));
    ap_assert(config != NULL);

    return config;
}

static void *create_dir_config(apr_pool_t *pool, char *context) {
    module_dir_config_t *config = (module_dir_config_t *) apr_pcalloc(pool, sizeof(module_dir_config_t));
    ap_assert(config != NULL);

    config->allowed_schemes = NULL;
    config->endpoint_defaults = NULL;
    config->endpoints = NULL;

    return config;
}

static void *merge_dir_config(apr_pool_t *pool, void *BASE, void *ADD) {
    module_dir_config_t *base = (module_dir_config_t *) BASE;
    module_dir_config_t *add = (module_dir_config_t *) ADD;
    module_dir_config_t *config = (module_dir_config_t *) apr_pcalloc(pool, sizeof(module_dir_config_t));
    ap_assert(config != NULL);

    if (add->allowed_schemes != NULL) {
        config->allowed_schemes = add->allowed_schemes;
    } else {
        config->allowed_schemes = base->allowed_schemes;
    }

    if (add->endpoint_defaults != NULL) {
        config->endpoint_defaults = add->endpoint_defaults;
    } else {
        config->endpoint_defaults = base->endpoint_defaults;
    }

    if (add->endpoints != NULL) {
        config->endpoints = add->endpoints;
    } else {
        config->endpoints = base->endpoints;
    }

    return config;
}

/**
 * Utility for incrementally parsing an options string in in the format "key1=value1,key2=value2".
 *
 * Each invocation of the function will read the next key-value pair from input and update the input pointer.
 * This updated input pointer can then be passed to the next invocation of the function.
 *
 * @param pool Used for memory allocations.
 * @param input Pointer to a string of input to be parsed.
 *              Updated to point to the first char after the returned key-value pair.
 * @param key Updated to point to a string containing the next key.
 * @param value Updated to point to a string containing the next value.
 * @return APR_SUCCESS on success, APR_EOF if the end of input has been reached and an error status otherwise.
 */
static apr_status_t read_next_option(apr_pool_t *pool, const char **input, char **key, char **value) {
    const char *eq_sign, *value_start, *value_end_delim;

    if (**input == '\x00')
        return APR_EOF;

    eq_sign = strchr(*input, '=');
    if (eq_sign == NULL)
        return DTPC_PARAMS_PARSING_ERROR;

    *key = apr_pstrndup(pool, *input, eq_sign - *input);
    if (key == NULL) return APR_ENOMEM;

    value_start = eq_sign + 1;

    value_end_delim = strchr(value_start, ',');
    if (value_end_delim == NULL)
        value_end_delim = strchr(value_start, '\x00');

    *value = apr_pstrndup(pool, value_start, value_end_delim - value_start);
    if (value == NULL) return APR_ENOMEM;

    *input = value_end_delim;
    if (*value_end_delim != '\x00')
        ++(*input);

    return APR_SUCCESS;
}

static const char *parse_endpoint_config_params(apr_pool_t *pool, const char *input, endpoint_options_stack_t *stack) {
    apr_status_t status;

    const char *input_it = input;
    char *key, *value;

    while ((status = read_next_option(pool, &input_it, &key, &value)) == APR_SUCCESS) {
        endpoint_options_stack_option_t option = {0};

        if (strcmp(key, "allowed") == 0) {
            option.key = DTPC_ECK_ALLOWED;
            if (strcmp(value, "all") == 0) {
                option.int_value = DTPC_ACTION_PUSH | DTPC_ACTION_PULL;
            } else if (strcmp(value, "push") == 0) {
                option.int_value = DTPC_ACTION_PUSH;
            } else if (strcmp(value, "pull") == 0) {
                option.int_value = DTPC_ACTION_PULL;
            } else if (strcmp(value, "none") == 0) {
                option.int_value = 0;
            } else {
                return apr_psprintf(pool, "Invalid value for 'allowed' parameter: %s", value);
            }
        } else if (strcmp(key, "copy_headers") == 0) {
            option.key = DTPC_ECK_COPY_HEADERS;
            if (strcmp(value, "all") == 0) {
                option.copy_headers_value = DTPC_COPY_HEADERS_ALL;
            } else if (strcmp(value, "authentication") == 0) {
                option.copy_headers_value = DTPC_COPY_HEADERS_AUTHENTICATION;
            } else if (strcmp(value, "none") == 0) {
                option.copy_headers_value = DTPC_COPY_HEADERS_NONE;
            } else {
                return apr_psprintf(pool, "Invalid value for 'copy_headers' parameter: %s", value);
            }
        } else if (strcmp(key, "checksum_algorithm") == 0) {
            option.key = DTPC_ECK_CHECKSUM_ALGORITHM;
            if (strcmp(value, "none") == 0) {
                option.string_value = NULL;
            } else {
                option.string_value = apr_pstrdup(pool, value);
            }
        } else {
            return apr_psprintf(pool, "Unknown parameter name: %s", key);
        }

        status = eos_add_option(stack, &option);
        ap_assert(status == APR_SUCCESS);
    }

    if (status != APR_EOF) {
        const char *err = dtpc_strerror_pool(pool, status);
        return apr_psprintf(pool, "Error while scanning input: %s", err);
    }

    return NULL;
}

static const char *set_allowed_schemes(cmd_parms *parms, void *mconfig, const char *scheme_spec) {
    const char *err;
    module_dir_config_t *dir_config;

    err = ap_check_cmd_context(parms, NOT_IN_HTACCESS);
    if (err) {
        return err;
    }

    dir_config = (module_dir_config_t *) mconfig;
    ap_assert(dir_config != NULL);

    dir_config->allowed_schemes = apr_array_make(parms->pool, 0, sizeof(scheme_config_t *));
    ap_assert(dir_config->allowed_schemes != NULL);

    if (strcasecmp(scheme_spec, "all") == 0 || strcasecmp(scheme_spec, "push:all") == 0 ||
            strcasecmp(scheme_spec, "pull:all") == 0) {
        int allowed_actions = 0;
        if (strcasecmp(scheme_spec, "all") == 0) {
            allowed_actions = DTPC_ACTION_PUSH | DTPC_ACTION_PULL;
        } else {
            const char *action = ap_getword(parms->temp_pool, &scheme_spec, ':');
            if (strcasecmp(action, "push") == 0) {
                allowed_actions = DTPC_ACTION_PUSH;
            } else if (strcasecmp(action, "pull") == 0) {
                allowed_actions = DTPC_ACTION_PULL;
            } else {
                return apr_psprintf(parms->temp_pool, "Unknown action \"%s\" passed to TPCAllowedSchemes for "
                                    "\"all\" scheme", action);
            }
        }

        // Add all known schemes to allowed list
        for (const scheme_t *scheme = schemes; scheme->name != NULL; ++scheme) {
            scheme_config_t *scheme_config = (scheme_config_t *) apr_pcalloc(parms->pool, sizeof(scheme_config_t));
            scheme_config->scheme = scheme->name;
            scheme_config->allowed_actions = allowed_actions;
            APR_ARRAY_PUSH(dir_config->allowed_schemes, scheme_config_t *) = scheme_config;
        }
    } else if (strcasecmp(scheme_spec, "none") == 0) {
        // leave allowed_schemes empty
    } else {
        const char *it = scheme_spec;
        while (1) {
            const char *field, *action, *scheme_name;

            scheme_config_t *scheme_config = (scheme_config_t *) apr_pcalloc(parms->pool, sizeof(scheme_config_t));

            field = ap_getword(parms->temp_pool, &it, ',');

            if (strchr(field, ':') != NULL) {
                // After ap_getword, scheme_name will contain everything after ':'
                scheme_name = field;
                action = ap_getword(parms->temp_pool, &scheme_name, ':');
                if (strcasecmp(action, "push") == 0) {
                    scheme_config->allowed_actions = DTPC_ACTION_PUSH;
                } else if (strcasecmp(action, "pull") == 0) {
                    scheme_config->allowed_actions = DTPC_ACTION_PULL;
                } else {
                    return apr_psprintf(parms->temp_pool, "Unknown action \"%s\" passed to TPCAllowedSchemes for "
                                        "scheme field \"%s\"", action, field);
                }
            } else {
                scheme_name = field;
                scheme_config->allowed_actions = DTPC_ACTION_PUSH | DTPC_ACTION_PULL;
            }

            const scheme_t *scheme = lookup_scheme_by_name(scheme_name);
            if (scheme == NULL) {
                return apr_psprintf(parms->temp_pool, "Unknown scheme \"%s\" passed to TPCAllowedSchemes for scheme "
                                    "field \"%s\"", scheme_name, field);
            }

            scheme_config->scheme = scheme->name;

            APR_ARRAY_PUSH(dir_config->allowed_schemes, scheme_config_t *) = scheme_config;

            if (*it == '\x00') {
                break;
            }
        }
    }

    return NULL;
}

static const char *set_default_endpoint_config(cmd_parms *parms, void *mconfig, const char *endpoint_config) {
    const char *err;
    module_dir_config_t *dir_config;

    err = ap_check_cmd_context(parms, NOT_IN_HTACCESS);
    if (err) {
        return err;
    }

    dir_config = (module_dir_config_t *) mconfig;
    ap_assert(dir_config != NULL);

    endpoint_options_stack_t *stack;
    apr_status_t status = eos_create(parms->pool, &stack);
    if (status != APR_SUCCESS) {
        return dtpc_strerror_pool(parms->pool, status);
    }

    err = parse_endpoint_config_params(parms->pool, endpoint_config, stack);
    if (err) {
        return err;
    }

    dir_config->endpoint_defaults = stack;

    return NULL;
}

static const char *set_endpoints_file(cmd_parms *parms, void *mconfig, const char *endpoints_file) {
    apr_status_t status;
    const char *err;
    module_dir_config_t *dir_config;

    err = ap_check_cmd_context(parms, NOT_IN_HTACCESS);
    if (err) {
        return err;
    }

    dir_config = (module_dir_config_t *) mconfig;
    ap_assert(dir_config != NULL);


    endpoints_options_map_t *map;
    status = esom_create(parms->pool, &map);
    if (status != APR_SUCCESS) {
        return apr_psprintf(parms->pool, "Internal error: %s", dtpc_strerror_pool(parms->pool, status));
    }

    char line[MAX_STRING_LEN];

    ap_configfile_t *f;
    status = ap_pcfg_openfile(&f, parms->pool, endpoints_file);
    if (status != APR_SUCCESS) {
        return apr_psprintf(parms->pool, "Could not open endpoints file %s: %s", endpoints_file,
                            dtpc_strerror_pool(parms->pool, status));
    }

    while (!(ap_cfg_getline(line, MAX_STRING_LEN, f))) {
        char *line_it = line;

        // Skip leading whitespace
        for (; apr_isspace(*line_it); ++line_it) {}

        // Skip commented (#) and empty lines
        if (line_it[0] == '#' || line_it[0] == '\0') {
            continue;
        }

        const char *endpoint_uri_str = ap_getword_white_nc(parms->pool, &line_it);

        apr_uri_t parsed_endpoint_uri;
        status = apr_uri_parse(parms->pool, endpoint_uri_str, &parsed_endpoint_uri);
        if (status != APR_SUCCESS) {
            return apr_psprintf(parms->pool, "Failed to parse URI \"%s\" (%s:%d): %s", endpoint_uri_str, f->name,
                                f->line_number, dtpc_strerror_pool(parms->pool, status));
        }

        if (lookup_scheme_by_name(parsed_endpoint_uri.scheme) == NULL) {
            return apr_psprintf(parms->pool, "Unknown scheme \"%s\" in URI \"%s\" (%s:%d)", parsed_endpoint_uri.scheme,
                                endpoint_uri_str, f->name, f->line_number);
        }

        apr_uri_t *endpoint_uri = apr_pcalloc(parms->pool, sizeof(apr_uri_t));
        status = extract_endpoint_uri_from_uri(parms->pool, &parsed_endpoint_uri, endpoint_uri);
        if (status != APR_SUCCESS) {
            return apr_psprintf(parms->pool, "Failed to extract endpoint from URI \"%s\" (%s:%d): %s",
                                endpoint_uri_str, f->name, f->line_number, dtpc_strerror_pool(parms->pool, status));
        }

        const char *unparsed_uri = endpoint_uri_to_str(parms->pool, endpoint_uri);
        if (strcmp(endpoint_uri_str, unparsed_uri) != 0) {
            return apr_psprintf(parms->pool, "Endpoint URI \"%s\" (%s:%d) is not in canonical representation, "
                                "should be \"%s\"", endpoint_uri_str, f->name, f->line_number, unparsed_uri);
        }

        endpoint_options_stack_t *stack;
        status = eos_create(parms->pool, &stack);
        if (status != APR_SUCCESS) {
            return apr_psprintf(parms->pool, "Internal error: %s", dtpc_strerror_pool(parms->pool, status));
        }

        err = parse_endpoint_config_params(parms->pool, line_it, stack);
        if (err != NULL) {
            return apr_psprintf(parms->pool, "Failed to parse params \"%s\" (%s:%d): %s", line_it, f->name,
                                f->line_number, err);
        }

        status = esom_add_endpoint(map, endpoint_uri, stack);
        if (status != APR_SUCCESS) {
            return apr_psprintf(parms->pool, "Internal error: %s", dtpc_strerror_pool(parms->pool, status));
        }
    }
    // On error, the process stops, so there is no need to close the file.
    ap_cfg_closefile(f);

    dir_config->endpoints = map;

    return NULL;
}

static const command_rec config_commands[] = {
    AP_INIT_TAKE1(
        "TPCAllowedSchemes", set_allowed_schemes, NULL, RSRC_CONF|ACCESS_CONF,
        "Allow-list a set of schemes for connecting to remote endpoints."
    ),
    AP_INIT_TAKE1(
        "TPCDefaultEndpointConfig", set_default_endpoint_config, NULL, RSRC_CONF|ACCESS_CONF,
        "Set the default configuration for endpoints."
    ),
    AP_INIT_TAKE1(
        "TPCEndpointsFile", set_endpoints_file, NULL, RSRC_CONF|ACCESS_CONF,
        "Read specific configuration for multiple endpoints from a file."
    ),
    { NULL }
};

static const char *log_field_transfer_mode(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL) {
        return "direct";
    }

    return apr_pstrcat(r->pool, "tpc:", action_e_to_name(req_config->summary->action), NULL);
}

static const char *log_field_transfer_url(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL) {
        return "-";
    }

    return apr_uri_unparse(r->pool, req_config->summary->remote_uri, 0);
}

static const char *log_field_transfer_endpoint(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL) {
        return "-";
    }

    return endpoint_uri_to_str(r->pool, req_config->summary->endpoint_uri);
}

static const char *log_field_transfer_status(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL) {
        return "-";
    }

    return tpc_status_e_to_name(req_config->summary->status);
}

static const char *log_field_transfer_bytes(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL) {
        return "-";
    }

    return apr_off_t_toa(r->pool, req_config->summary->bytes_transferred);
}

static const char *log_field_transfer_error_code(request_rec *r, char *a) {
    module_request_config_t *req_config = ap_get_module_config(r->request_config, &dav_tpc_module);
    if (req_config == NULL || req_config->summary == NULL || req_config->summary->status != DTPC_STATUS_FAILED) {
        return "-";
    }

    if (req_config->summary->error_status > APR_OS_START_SYSERR) {
        return apr_itoa(r->pool, APR_FROM_OS_ERROR(req_config->summary->error_status));
    } else {
        return apr_itoa(r->pool, req_config->summary->error_status);
    }
}

static int register_log_handlers(apr_pool_t *p, apr_pool_t *plog, apr_pool_t *ptemp) {
    APR_OPTIONAL_FN_TYPE(ap_register_log_handler) *register_log_handler;

    register_log_handler = APR_RETRIEVE_OPTIONAL_FN(ap_register_log_handler);

    if (register_log_handler) {
        register_log_handler(p, "^TM", log_field_transfer_mode, 0);
        register_log_handler(p, "^TU", log_field_transfer_url, 0);
        register_log_handler(p, "^TE", log_field_transfer_endpoint, 0);
        register_log_handler(p, "^TS", log_field_transfer_status, 0);
        register_log_handler(p, "^TB", log_field_transfer_bytes, 0);
        register_log_handler(p, "^TC", log_field_transfer_error_code, 0);
    }

    return OK;
}


static void register_hooks(apr_pool_t *pool) {
    ap_hook_pre_config(register_log_handlers, NULL, NULL, APR_HOOK_REALLY_FIRST);

    // Our hook must run before mod_dav's hook.
    static const char *const run_hook_before_modules[] = { "mod_dav.c", NULL };
    ap_hook_handler(handle_tpc_decline_otherwise, NULL, run_hook_before_modules, APR_HOOK_MIDDLE);
}

AP_DECLARE_MODULE(dav_tpc) = {
    STANDARD20_MODULE_STUFF,
    create_dir_config,
    merge_dir_config,
    create_server_config,
    NULL,
    config_commands,
    register_hooks
};

// FUTR: Allow admin to configure scheme to port mappings, or read /etc/services
// FUTR: For endpoint configuration, distinguish between TCP and UDP? Probably not necessary

// FUTR: Check with GFAL2 whether a given URL is supported?
//       This is possible with gfal_find_plugin(), however this function is not supposed to be
//       used outside of GFAL2 itself. Perhaps simply perform a stat call

// FUTR: Config option to answer a TPC COPY with a redirect to another host

// FUTR: Checksumming integrated with other modules (Want-Digest, ...)
//       The copy operation always starts with the source checksum
//       calculation/fetching (with the manually specified algorithm). After
//       the transfer, checksum calculation/fetching on the destination is
//       with the same algorithm is triggered.
//       Unfortunately, there is no way to detect which algorithms are
//       supported via HTTP Want-Digest.

// TODO: GFAL2 returns EEXISTS from COPY when the PUT returns 409 (because a
//       parent collection is missing) but it should rather return ENOENT.

// TODO: Checks on source and destination in order to generate proper error codes?
//       Checks would be "active", resulting in syscalls or gfal2 calls
//       E.g. source must not be a directory/collection
//       Remote URI must have a plugin available
//       Local file must be accessible

// TODO: Reject requests containing a body, check out request_rec.read_body = REQUEST_NO_BODY
