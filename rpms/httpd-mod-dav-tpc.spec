%global upstream_version 0.1.0
%global package_release 1

Summary:    mod_dav_tpc for Apache HTTP Server
Name:       httpd-mod-dav-tpc
Version:    %( echo %upstream_version | sed -E 's/-(beta|alpha|rc)/~\1/i' | tr '-' '.' )
Release:    %{package_release}%{?dist}
URL:        https://codebase.helmholtz.cloud/kit-scc-sdm/onlinestorage/httpd-webdav/mod-dav-tpc
License:    ASL 2.0
Group:      System Environment/Daemons

Source0:    mod_dav_tpc.tar.gz
Source1:    README.md

BuildRequires:  gcc
BuildRequires:  make
# According to RHBZ #1059143, httpd-2.4.6-21 has some backported patches
%if 0%{?el7}
BuildRequires:  httpd-devel >= 2.4.6-21.el7
Requires:       httpd >= 2.4.6-21.el7
%else
BuildRequires:  httpd-devel >= 2.4.7
Requires:       httpd >= 2.4.7
%endif
BuildRequires:  which
BuildRequires:  gfal2-devel


%description
The Apache HTTP Server is a powerful, efficient, and extensible web server.

This package contains the mod_dav_tpc module which adds Third-Party-Copy (TPC)
capabilities to the existing WebDAV module (mod_dav and mod_dav_fs).

%prep
%setup -q -n mod_dav_tpc

%build
# Do not need configure
make %{?_smp_mflags}


%install
rm -rf %{buildroot}

install -m 644 %{SOURCE1} .
install -D .libs/mod_dav_tpc.so %{buildroot}/%{_httpd_moddir}/mod_dav_tpc.so
install -d %{buildroot}/%{_httpd_modconfdir}/

cat > %{buildroot}/%{_httpd_modconfdir}/dav_tpc.load << EOF
# mod_dav_tpc adds Third-Party-Copy (TPC) capabilities to WebDAV.
#LoadModule dav_tpc_module modules/mod_dav_tpc.so
EOF


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README.md
%{_httpd_moddir}/mod_dav_tpc.so
%config(noreplace) %{_httpd_modconfdir}/dav_tpc.load

%changelog
* Fri Jun 23 2023 Paul Skopnik <paul.skopnik@kit.edu> - 0.1.0-1
- First release
