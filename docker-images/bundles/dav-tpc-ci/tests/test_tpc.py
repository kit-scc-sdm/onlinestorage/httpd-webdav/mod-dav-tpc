from dataclasses import dataclass
from enum import auto, Enum
from typing import Optional

import pytest


class Direction(Enum):
    PUSH = auto()
    PULL = auto()


class TPCStatus(Enum):
    SUCCESS = auto()
    FAILED = auto()


@dataclass
class TPCResult:
    tpc_status: TPCStatus
    http_status_code: int
    http_error_message: Optional[str] = None
    perf_marker_stream: Optional[str] = None
    tpc_failed_message: Optional[str] = None

    @property
    def succeeded(self):
        return self.tpc_status == TPCStatus.SUCCESS


def _parse_perf_marker_stream(text, result: TPCResult):
    result.perf_marker_stream = text

    status_set = False
    in_report = False
    for line in text.splitlines():
        if in_report:
            if line.lower() == 'end':
                in_report = False
        else:
            if line.lower() == 'perf marker':
                in_report = True
            elif line.lower().startswith('success'):
                in_report = True
                result.tpc_status = TPCStatus.SUCCESS
                status_set = True
            elif line.lower().startswith('failed'):
                in_report = True
                result.tpc_status = TPCStatus.FAILED
                result.tpc_failed_message = line
                status_set = True

    if not status_set:
        raise Exception('Perf Marker Stream did not contain summary')

def _perform_tpc(session, src, dest, direction=Direction.PUSH, headers={}, transfer_headers={}):
    if direction == Direction.PUSH:
        url = src
        copy_headers = {'Destination': dest}
    elif direction == direction.PULL:
        url = dest
        copy_headers = {'Source': src}

    transformed_transfer_headers_ = dict(
        (f'TransferHeader{key}', val) for key, val in transfer_headers.items()
    )

    final_headers = transformed_transfer_headers_ | headers | copy_headers

    r = session.request('COPY', url, headers=final_headers)

    if r.status_code == 202:
        if 'text/perf-marker-stream' not in r.headers['Content-Type'].split(';'):
            raise Exception('Unexpected response type for TPC request')

        result = TPCResult(TPCStatus.SUCCESS, r.status_code)
        _parse_perf_marker_stream(r.text, result)
        return result

    else:
        return TPCResult(TPCStatus.FAILED, r.status_code, http_error_message=r.text)


def test_direct(httpd_base_url, session):
    # Dummy test to check that both HTTPDs are reachable

    r = session.get(httpd_base_url / 'source/content.txt')
    assert r.status_code == 200

    r = session.get('http://httpd-passive/source/content.txt')
    assert r.status_code == 200

def test_tpc_basic(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/test_basic.txt',
    )
    assert r.succeeded

def test_tpc_endpoints_file(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/endpoints-file/content.txt',
        'http://httpd-passive/destination/test_endpoints_file.txt',
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/content.txt',
        'http://httpd-passive/destination/test_endpoints_file_2.txt',
    )
    assert not r.succeeded
    assert r.http_status_code == 405

    # Endpoints file only allows https://example.com:443
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/endpoints-file/content.txt',
        'http://example.com/test_endpoints_file.txt',
    )
    assert not r.succeeded
    assert r.http_status_code == 405

def test_tpc_only_push(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/only-push/content.txt',
        'http://httpd-passive/destination/test_only_push.txt',
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        'http://httpd-passive/destination/test_only_push.txt',
        httpd_base_url / 'source/only-push/pull_dest.txt',
        direction = Direction.PULL,
    )
    assert not r.succeeded
    assert r.http_status_code == 405

def test_tpc_pull(httpd_base_url, session):
    r = _perform_tpc(
        session,
        'http://httpd-passive/source/basic/content.txt',
        httpd_base_url / 'destination/only-pull/pull_dest.txt',
        direction = Direction.PULL,
    )
    assert r.succeeded

def test_tpc_only_pull(httpd_base_url, session):
    r = _perform_tpc(
        session,
        'http://httpd-passive/source/basic/content.txt',
        httpd_base_url / 'destination/only-pull/pull_dest.txt',
        direction = Direction.PULL,
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'destination/only-pull/pull_dest.txt',
        'http://httpd-passive/destination/only_pull_dest.txt',
        direction = Direction.PULL,
    )
    assert not r.succeeded
    assert r.http_status_code == 405

def test_tpc_overwrite(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/overwrite_1.txt',
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/overwrite_1.txt',
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/overwrite_1.txt',
        headers = {'Overwrite': 'F'},
    )
    assert not r.succeeded
    assert r.http_status_code == 412

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/secondary-content.txt',
        'http://httpd-passive/destination/overwrite_2.txt',
        headers = {'Overwrite': 'T'},
    )
    assert r.succeeded

def test_tpc_digest(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/check-digest/content.txt',
        'http://httpd-passive/destination/respond-with-digest/content.txt',
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/check-digest/secondary-content.txt',
        'http://httpd-passive/destination/respond-with-digest/secondary-content.txt',
    )
    assert not r.succeeded
    assert 'DESTINATION CHECKSUM MISMATCH' in r.tpc_failed_message

def test_tpc_authn_headers(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/copy-authn/content.txt',
        'http://httpd-passive/destination/require-authn/content.txt',
        transfer_headers = {'Authorization': 'Bearer MyToken'},
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/copy-authn/content.txt',
        'http://httpd-passive/destination/require-authn/content.txt',
    )
    assert not r.succeeded
    assert r.http_status_code == 403

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/require-authn/content.txt',
    )
    assert not r.succeeded
    assert r.http_status_code == 403

def test_tpc_all_headers(httpd_base_url, session):
    r = _perform_tpc(
        session,
        httpd_base_url / 'source/copy-headers/content.txt',
        'http://httpd-passive/destination/require-headers/content.txt',
        transfer_headers = {
            'Authorization': 'Bearer MyToken',
            'CustomHeader': 'Custom Value',
        },
    )
    assert r.succeeded

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/copy-headers/content.txt',
        'http://httpd-passive/destination/require-headers/content.txt',
        transfer_headers = {'Authorization': 'Bearer MyToken'},
    )
    assert not r.succeeded
    assert r.http_status_code == 403

    r = _perform_tpc(
        session,
        httpd_base_url / 'source/basic/content.txt',
        'http://httpd-passive/destination/require-headers/content.txt',
    )
    assert not r.succeeded
    assert r.http_status_code == 403
