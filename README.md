# mod_dav_tpc

This module adds Third-Party-Copying (TPC) capabilities to Apache HTTPD in combination with `mod_dav` and `mod_dav_fs`.
The implementation follows the [dCache HTTP-TPC specification](https://github.com/dCache/HTTP-TPC).
Targeted and tested TPC clients are [FTS3](https://fts.web.cern.ch/fts/) and [GFAL2](https://gitlab.cern.ch/dmc/gfal2).

The data transfer itself is performed via GFAL2 which supports multiple transfer protocols.
However, only HTTP/HTTPS/WebDAV are officially supported.

## Configuration and Concepts

### Endpoints

_Endpoints_ are a central concept within `mod_dav_tpc`.
They represent a single logical service which provides access to a storage system.
Both configuration and logs commonly refer to endpoints.

Each endpoint is distinctly identified by a reduced URI comprising the scheme/protocol and the network address made up of host and port.
For example `http://example.com:80` or `https://example.com:443`.
A fully specified endpoint URI contains all three components explicitly.

#### Detection of Remote Endpoints

`mod_dav_tpc` inspects each request before handing over to `mod_dav`/`mod_dav_fs`.
Only `COPY` requests with a remote source or destination URL are handled by `mod_dav_tpc` and all other requests are handed over to the regular WebDAV modules without action.

Hence, `mod_dav_tpc` must determine whether the endpoint extracted from the URL of the "other end" (one of the `Source` or the `Destination` header) of the `COPY` is the current endpoint or a remote endpoint.
The module looks at VirtualHost which handles the process to determine its hostname and port.
If both do not match the extracted URL, the extracted endpoint in the request is remote and the `COPY` is a TPC.

This technique requires the VirtualHost to be explicitly defined.
To avoid any ambiguities, it should be configured so that the hostname and port are clear:

´´´
<VirtualHost *:80>
    ServerName webdav.example.com
</VirtualHost>
´´´

#### Endpoint Configuration

For each TPC request, `mod_dav_tpc` dynamically builds a single configuration of the associated endpoint.
The configuration affects how exactly the TPC operation is executed.

The configuration consists of these fields:

 * `allowed`: TPC actions which are allowed on the endpoint.
   Values are `none` (no TPC), `push` (only TPC push), `pull` (only TPC push), `all` (all kinds of TPC).
   The default is `none`.
 * `copy_headers`: Set of transfer headers (`TransferHeader<Name>`) to copy to regular headers in the `GET`/`PUT` request.
   Values are `none` (no headers), `authentication` (only authentication related headers, such as the token from an `Authorization` header), `all` (all headers).
   The default is `none`.
 * `checksum_algorithm`: Name of the checksum algorithm used to verify data integrity after the transfer.
   Values are `none` (do not perform checksum verification) or the name of a checksum algorithm.
   The default is `none`.
   See section [Checksumming](#checksumming).

The `allowed` field allows to limit TPC to known endpoints by either allow-listing specific endpoints (recommended) or deny-listing specific endpoints (discouraged).

The endpoint configuration is built by combining multiple sources.
All sources can specify only some fields to overwrite, with values for unspecified fields being inherited:

 1. Hardcoded default config (see default values for each field)
 2. Configured default config (see directive `TPCDefaultEndpointConfig`)
 3. Endpoint specific configuration (see directive `TPCEndpointsFile`)

### Schemes and Protocols

GFAL2 supports multiple protocols through its plugin system.
When performing a GFAL2 operation, all plugins are asked to decide whether they can handle the involved URL.
There is no simple way to limit the plugins which are loaded, so `mod_dav_tpc` instead introduces limits via URL schemes.

At the moment the set of all supported schemes is hardcoded in `mod_dav_tpc`.
In addition, the set of allowed schemes can be limited through the `TPCAllowedSchemes` directive (recommended).

### Checksumming

Commonly a checksum is calculated from the source and destination of a data transfer, to verify the integrity of the data.
This step may be performed by the program initiating the transfer, e.g. the TPC client such as FTS3.

Optionally, the `mod_dav_tpc` module can also perform checksums as part of the active TPC operation.
This requires configuring the checksumming algorithm for the endpoint.
The endpoint must support the algorithm or the TPC operation will fail.

Checksumming is provided by GFAL2 and the following algorithms are supported for the local file:

 * `none` - No checksumming (the default).
 * `md5` - MD5
 * `adler32` - ADLER32
 * `crc32` - CRC32

### Log Fields

`mod_dav_tpc` adds several fields which are available in the log file produced by `CustomLog` ("access log"), which is configured through `LogFormat`.

 * `%^TM`: Transfer mode, one of: `direct` (not a TPC request), `tpc:push`, `tpc:pull`.
 * `%^TU`: TPC remote URL or `-` for a non-TPC request.
 * `%^TE`: TPC remote endpoint URL or `-` for a non-TPC request.
 * `%^TS`: TPC final status, one of: `success`, `failed`, `-` (not a TPC request).
 * `%^TB`: Number of bytes transferred via TPC or `-` for a non-TPC request.
   This number is sometimes not accurate and `0` is given for small files.
 * `%^TC`: Error code if the TPC request failed or `-` if the status is not `failed`.
   This is either a regular OS error code ([`errno(3)`](https://man7.org/linux/man-pages/man3/errno.3.html), meaning can be checked with `errno` command) if values are less than 20000 or an [apr_status_t](https://apr.apache.org/docs/apr/1.4/group__apr__errno.html) value.

To have the same output log format on HTTPD instances where `mod_dav_tpc` is loaded and where `mod_dav_tpc` isn't loaded, the default value of each field can be used:

```
# On HTTPD where mod_dav_tpc is loaded, use the log fields:
LogFormat "... %^TM %^TU %^TE %^TS %^TB %^TC" webdav_tpc

# On HTTPD where mod_dav_tpc is not loaded, use the default value of fields:
LogFormat "... direct - - - - -" webdav_no_tpc
```

### Enabling the Module

Enabling `mod_dav_tpc` on an HTTPD instance requires loading the module the usual way:

```
LoadModule dav_tpc_module modules/mod_dav_tpc.so
```

No further global configuration is required.

`mod_dav_tpc` also requires WebDAV to be loaded via `mod_dav` and `mod_dav_fs`.
See also the section on [Runtime Dependencies](#runtime-dependencies).

The module only takes action when the WebDAV handler is enabled on a directory.
It only makes sense with the `filesystem` provider (`mod_dav_fs`).

```
<Directory /...>
    Dav On
    # ...
</Directory>
```

By default, all TPC requests will be rejected.
At least the directives `TPCAllowedSchemes` and `TPCDefaultEndpointConfig` must be in place to perform any TPC.

### Directives

#### `TPCAllowedSchemes`

`TPCAllowedSchemes [push:|pull:]all|none|[push:|pull:]<scheme>,...`

Contexts: virtual host, directory

Configures the set of allowed schemes for the TPC data transfer.
These correspond to the URL scheme of the remote URL in the TPC request.
See the section on [Schemes and Protocols](#schemes-and-protocols).

The `push:`/`pull:` prefix optionally configures that a particular scheme may only be used for push TPC (`mod_dav_tpc` uploads to the remote endpoint) or pull TPC (`mod_dav_tpc` downloads from the remote endpoint).
No prefix means both are allowed.

If a TPC request is received which uses a scheme which is not allowed, the request is rejected before any endpoint-specific configuration is built or evaluated.

The default is equivalent to setting `none`.
When specifying this directive again for a more specific directory, the previous setting is fully overwritten.

#### `TPCDefaultEndpointConfig`

`TPCDefaultEndpointConfig <key>=<value>,...`

Contexts: virtual host, directory

Configures the default configuration for endpoints in TPC requests on files in the directory.
See [Endpoint Configuration](#endpoint-configuration) for the available fields and combination with other configuration sources.
Not all fields have to be set.

If no `TPCDefaultEndpointConfig` directive is in effect, all fields in the endpoint configuration are set through other sources.

When specifying this directive again for a more specific directory, the previous setting is fully overwritten.
That means setting only `allowed=all` at a general directory and only `copy_headers=all` at a more specific directory is not merged at the more specific directory.
Instead, only `copy_headers=all` is in effect.

#### `TPCEndpointsFile`

`TPCEndpointsFile <filepath.conf>`

Contexts: virtual host, directory

Configures a file to read containing endpoint specific configuration which is considered for endpoints in TPC requests on files in the directory.
See [Endpoint Configuration](#endpoint-configuration) for combination with other configuration sources.

If no `TPCEndpointsFile` directive is in effect, all fields in the endpoint configuration are set via other sources.

When specifying this directive again for a more specific directory, the previous setting is fully overwritten.
That means when no merging of the lists of endpoints takes place.

The `TPCDefaultEndpointConfig` and `TPCDefaultEndpointConfig` directives are independent.
For each directory, the closest matching of the individual directives takes effect.

The file is read only once during configuration parsing.
That means that HTTPD has to be at least reloaded (graceful, no downtime) for any changes to be picked up.

#### Endpoints File Format

The file specified in `TPCEndpointsFile` must follow a simple text-based format.

Each line contains configuration for a single endpoint.
Comment lines (starting with `#`) and whitespace at the beginning of lines are allowed.

```
<endpoint_url> <key>=<value>,...
```

See [Endpoint Configuration](#endpoint-configuration) for the available fields.
Not all fields have to be set.

## Security Considerations

Third Party Copying (TPC) introduces significant vectors for potential abuse and attack.
Careful evaluation of the protocol extension and this module is recommended before deployment.

In particular, the very core of the feature is [Server Side Request Forgery (SSRF)](https://owasp.org/www-community/attacks/Server_Side_Request_Forgery).
SSRF may be used to amplify and conceal attacks as well as circumvent network access policies in the presence of firewalls.
**Allow-listing only known endpoints for TPC operations is recommended**

Additionally, the GFAL2 library utilises a plugin system for supporting a multitude of data protocols.
All these plugins are also exposed through `mod_dav_tpc`.
**Allow-listing only well-understood schemes for data transfers such as HTTPS is recommended.**

Also see the [dCache HTTP-TPC specification](https://github.com/dCache/HTTP-TPC) for further security considerations.

## Runtime Dependencies

GFAL2 (`libgfal2.so`), and GFAL2 Transfer (`libgfal_transfer.so`) and at least `file` and `http` plugin must be installed on the system.

Both `mod_dav` and `mod_dav_fs` must be loaded in HTTPD.

## Releases, Packaging and Distribution

`mod_dav_tpc` is released through tags in the Git repository.
The versioning scheme follows [Semantic Versioning 2.0.0](https://semver.org/).

RPMs are built in GitLab CI and made available as job artifacts.
These artifacts can be downloaded from the "Tags" page in GitLab by clicking the download icon next to a version tag and selecting the appropriate job, e.g. `package:rpm:el8` for Enterprise Linux 8.
Versions of the RPMs correspond to the versions of the module.

A module builder Docker image is also built, but there is no tagging in place yet.
See the test configurations for how to use the image and see the Docker Basics repository for background.

### For Maintainers: Releasing

Releasing a new version requires amending the version in several places:

 * `mod_dav_tpc/mod_dav_tpc.c`: `DAV_TPC_VERSION`
 * `rpms/httpd-mod-dav-tpc.spec`: `upstream_version`

These changes have to be committed and this commit tagged with `v${VERSION}`.

## Developing and Testing

Docker images are built for development and testing.
See the Docker Basics repository for background.

Special about this project is that a second HTTPD container is required to be able to transfer data.
This container should be named `httpd-passive` to match the hostname used in CI testing.
Optionally, the passive HTTPD container endpoint can be network rate limited to 800 kbps (100 KiB/s) uplink and downlink, which makes it easier to test the perf-marker-stream:

```shell
# Start HTTPD directly without network rate limits
$ docker container run -it --rm --net httpd-webdav-pytest --cap-add NET_ADMIN --name httpd-passive registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/mod-dav-tpc/bundles/dav-tpc-ci
# Alternatively: Start HTTPD after setting up network rate limits
$ docker container run -it --rm --net httpd-webdav-pytest --cap-add NET_ADMIN --name httpd-passive registry.hzdr.de/kit-scc-sdm/onlinestorage/httpd-webdav/mod-dav-tpc/bundles/dav-tpc-ci bash
(container) $ apt update && apt install -y wondershaper
(container) $ wondershaper eth0 800 800
(container) $ httpd-foreground
```

Some additional larger files are required even with rate limiting in place so that the transfer runs long enough to emit performance markers:

```shell
# Start the primary HTTPD via Docker Basics tooling
$ webdav-docker run-server docker-images/bundles/dav-tpc-ci
# In another terminal
$ docker exec -it httpd bash
(container) $ head -c 1M /dev/urandom > /usr/local/apache2/htdocs/source/basic/small
(container) $ head -c 10M /dev/urandom > /usr/local/apache2/htdocs/source/basic/medium
```

Finally, start a shell session within a new container and issue requests:

```shell
$ webdav-docker pytest-shell docker-images/bundles/dav-tpc-ci
(container) $ curl -v -X COPY -H 'Destination: http://httpd-passive/destination/test1' "http://httpd/source/basic/small"
```

## Licensing

Except if explicitly noted otherwise, all content in this repository is licensed under the Apache License, Version 2.0 (see LICENSE file).
Detailed authorship information is part of the Git commit data or included in the files directly.

```
Copyright 2022-2023 The Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
